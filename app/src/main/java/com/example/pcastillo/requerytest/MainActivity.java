package com.example.pcastillo.requerytest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.example.pcastillo.requerytest.model.ChildEntity;
import com.example.pcastillo.requerytest.model.Models;
import com.example.pcastillo.requerytest.model.ParentEntity;
import io.requery.Persistable;
import io.requery.android.sqlite.DatabaseSource;
import io.requery.meta.EntityModel;
import io.requery.sql.*;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EntityModel model = Models.DEFAULT;
        DatabaseSource dataSource = new CustomDatabaseSource(this, model, 1);
        dataSource.setLoggingEnabled(true);

        Configuration configuration = dataSource.getConfiguration();

        SchemaModifier schemaModifier = new SchemaModifier(configuration);
        schemaModifier.createTables(TableCreationMode.DROP_CREATE);

        EntityDataStore<Persistable> dataStore = new EntityDataStore<>(configuration);

        ChildEntity childEntity1 = new ChildEntity();
        ChildEntity childEntity2 = new ChildEntity();
        ChildEntity childEntity3 = new ChildEntity();
        ChildEntity childEntity4 = new ChildEntity();
        ChildEntity childEntity5 = new ChildEntity();

        childEntity1.setName("Alpha");
        childEntity2.setName("Bravo");
        childEntity3.setName("Charlie");
        childEntity4.setName("Delta");
        childEntity5.setName("Echo");

        ParentEntity parentEntity = new ParentEntity();
        parentEntity.setName("Mother");

        parentEntity.getChildren().add(childEntity1);
        parentEntity.getChildren().add(childEntity2);
        parentEntity.getChildren().add(childEntity3);
        parentEntity.getChildren().add(childEntity4);
        parentEntity.getChildren().add(childEntity5);
        dataStore.insert(parentEntity);
    }
}
