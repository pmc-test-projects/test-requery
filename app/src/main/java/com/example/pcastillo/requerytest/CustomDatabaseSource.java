package com.example.pcastillo.requerytest;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import io.requery.android.sqlite.DatabaseSource;
import io.requery.meta.EntityModel;

/**
 * Created by pcastillo on 5/30/17.
 */
public class CustomDatabaseSource extends DatabaseSource {

    public CustomDatabaseSource(Context context, EntityModel model, int version) {
        super(context, model, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        super.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
    }
}
