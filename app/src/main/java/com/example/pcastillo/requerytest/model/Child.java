package com.example.pcastillo.requerytest.model;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.os.Parcelable;

import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.Persistable;

/**
 * Created by pcastillo on 5/30/17.
 */

@Entity
public interface Child extends Observable, Parcelable, Persistable {

    @Key
    @Generated
    int getId();

    @Bindable
    String getName();

    @Bindable
    @ManyToOne
    Parent getParent();
}
