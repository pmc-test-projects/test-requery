package com.example.pcastillo.requerytest.model;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.os.Parcelable;

import java.util.List;

import io.requery.CascadeAction;
import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.OneToMany;
import io.requery.Persistable;

/**
 * Created by pcastillo on 5/30/17.
 */

@Entity
public interface Parent extends Observable, Parcelable, Persistable {

    @Key
    @Generated
    int getId();

    @Bindable
    String getName();

    @OneToMany(mappedBy = "parent", cascade = {CascadeAction.DELETE, CascadeAction.SAVE})
    List<Child> getChildren();
}
